(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n  <div class=\"picnic\">\n    \n    <img class=\"picnic_image\" src=\"assets/picnic.png\" alt=\"\">\n  </div>\n  <div class=\"settings\">\n    <h3>Scegli il tuo giocatore</h3>\n    <div class=\"player\" *ngFor=\"let pl of colors; let i=index\" [ngStyle]=\"{'background-color': colors[i]}\" cdkDrag></div>\n    <img (click)=\"getCard()\" class=\"card_title\" src=\"assets/parole_proibite_titolo.png\" alt=\"\">\n    <img class=\"card\" *ngIf=\"card\" [src]=\"card\" alt=\"\">\n    <div class=\"dadi\">\n      <app-dadi></app-dadi>\n    </div>\n  </div>\n</div>\n  \n  \n\n\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  display: flex;\n  flex-direction: row; }\n  .container .picnic {\n    max-width: 1170px;\n    min-width: 900px; }\n  .container .picnic img {\n      max-width: 100%; }\n  .container .settings {\n    width: 300px;\n    position: relative; }\n  .container .card_title {\n    width: 80%;\n    display: block;\n    position: relative;\n    margin-left: 10px;\n    margin-top: 20px; }\n  .container .dadi {\n    width: 100%;\n    height: 200px;\n    position: relative;\n    margin-top: 190px; }\n  .container h3 {\n    font-family: 'Quicksand', sans-serif;\n    color: #383838;\n    margin-top: 50px;\n    margin-left: 10px; }\n  .container .player {\n    width: 30px;\n    height: 30px;\n    border-radius: 100%;\n    display: inline-block;\n    margin: 5px 10px; }\n  .container .parole_proibite {\n    position: absolute;\n    top: 282px;\n    width: 173px;\n    height: 34px;\n    left: 929px;\n    transform: rotate(-2.7deg);\n    cursor: pointer; }\n  .container .card {\n    position: absolute;\n    max-width: 95%;\n    margin-top: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYXJ0aW5hL0Rlc2t0b3AvcGljbmljL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksYUFBYTtFQUNiLG1CQUFtQixFQUFBO0VBRnZCO0lBSVEsaUJBQWlCO0lBQ2pCLGdCQUFnQixFQUFBO0VBTHhCO01BT1ksZUFBZSxFQUFBO0VBUDNCO0lBWVEsWUFBWTtJQUNaLGtCQUFrQixFQUFBO0VBYjFCO0lBaUJRLFVBQVU7SUFDVixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQTtFQXJCeEI7SUF5QlEsV0FBVztJQUNYLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsaUJBQWlCLEVBQUE7RUE1QnpCO0lBZ0NRLG9DQUFvQztJQUNwQyxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGlCQUFpQixFQUFBO0VBbkN6QjtJQXVDUSxXQUFXO0lBQ1gsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsZ0JBQWdCLEVBQUE7RUEzQ3hCO0lBK0NRLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0lBQ1gsMEJBQTBCO0lBQzFCLGVBQWUsRUFBQTtFQXJEdkI7SUF5RFEsa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmNvbnRhaW5lcntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgLnBpY25pY3tcbiAgICAgICAgbWF4LXdpZHRoOiAxMTcwcHg7XG4gICAgICAgIG1pbi13aWR0aDogOTAwcHg7XG4gICAgICAgIGltZ3tcbiAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5zZXR0aW5nc3tcbiAgICAgICAgd2lkdGg6IDMwMHB4OyAgIFxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuXG4gICAgLmNhcmRfdGl0bGV7XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cblxuICAgIC5kYWRpe1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBtYXJnaW4tdG9wOiAxOTBweDtcbiAgICB9XG5cbiAgICBoM3tcbiAgICAgICAgZm9udC1mYW1pbHk6ICdRdWlja3NhbmQnLCBzYW5zLXNlcmlmO1xuICAgICAgICBjb2xvcjogIzM4MzgzODtcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgfVxuXG4gICAgLnBsYXllcntcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBtYXJnaW46IDVweCAxMHB4O1xuICAgIH1cblxuICAgIC5wYXJvbGVfcHJvaWJpdGV7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAyODJweDtcbiAgICAgICAgd2lkdGg6IDE3M3B4O1xuICAgICAgICBoZWlnaHQ6IDM0cHg7XG4gICAgICAgIGxlZnQ6IDkyOXB4O1xuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgtMi43ZGVnKTtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIC5jYXJke1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIG1heC13aWR0aDogOTUlO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cblxuXG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'picnic';
        this.player = new Array(10);
        this.colors = ['#0097A7', '#FFC107', '#FF4081', '#4CAF50', '#FF5722', '#3F51B5', '#607D8B', '#5d4037', '#8e24aa', '#b71c1c'];
    }
    AppComponent.prototype.getCard = function () {
        var r_number = Math.floor(Math.random() * 15) + 1;
        this.card = 'assets/card_' + r_number + '.png';
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _dadi_dadi_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dadi/dadi.component */ "./src/app/dadi/dadi.component.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _dadi_dadi_component__WEBPACK_IMPORTED_MODULE_6__["DadiComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dadi/dadi.component.html":
/*!******************************************!*\
  !*** ./src/app/dadi/dadi.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"viewport\">\n  <div (click)=\"rollToIndex()\" *ngFor=\"let d of dadi; let i = index\" [ngClass]=\"(animation[i]) ? 'bis' : '' \" class=\"cube {{status[i]}}\">\n    <div  class=\"side side-1\">\n      <div></div>\n    </div>\n    <div  class=\"side side-2\">\n      <div></div>\n    </div>\n    <div  class=\"side side-3\">\n      <div></div>\n    </div>\n    <div  class=\"side side-4\">\n      <div></div>\n    </div>\n    <div class=\"side side-5\">\n      <div></div>\n    </div>\n    <div class=\"side side-6\">\n      <div></div>\n    </div>\n  </div>\n</div>\n<h3>*clicca sul dado per tirare</h3>"

/***/ }),

/***/ "./src/app/dadi/dadi.component.scss":
/*!******************************************!*\
  !*** ./src/app/dadi/dadi.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  font-family: 'Work Sans', sans-serif;\n  height: 100vh;\n  overflow: hidden;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  background: #ffffff;\n  color: #fa2d7d;\n  color: #575344; }\n\n[onclick] {\n  outline: none;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.viewport {\n  perspective: 350px;\n  transform: scale(0.7);\n  transform: translateY(-50%), scale(0.7);\n  text-align: center; }\n\n.cube {\n  font-size: .5em;\n  position: relative;\n  margin: 0 auto;\n  height: 10em;\n  width: 10em;\n  transform-style: preserve-3d;\n  transform: rotateX(0deg) rotateY(0deg) rotateZ(0deg);\n  transition: transform 1s ease-out;\n  display: inline-block;\n  margin: 4em; }\n\n.cube > div {\n  overflow: hidden;\n  position: absolute;\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: contain;\n  height: 10em;\n  width: 10em;\n  border: 0.3em solid;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  box-shadow: 0 0 1em rgba(0, 0, 0, 0.1);\n  border-radius: 0.5em;\n  outline: 1px solid transparent;\n  cursor: pointer; }\n\n.cube > div.side-1 {\n    background-image: url('faccia_dita_01-d.png'); }\n\n.cube > div.side-2 {\n    background-image: url('faccia_dita_02-d.png'); }\n\n.cube > div.side-3 {\n    background-image: url('faccia_dita_03-d.png'); }\n\n.cube > div.side-4 {\n    background-image: url('faccia_dita_04-d.png'); }\n\n.cube > div.side-5 {\n    background-image: url('faccia_dita_05-d.png'); }\n\n.cube > div.side-6 {\n    background-image: url('faccia_dita_06-d.png'); }\n\n.cube > div > div {\n  font-size: 4em;\n  font-weight: 600;\n  width: 1em;\n  height: 1em;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-top: -.5em;\n  margin-left: -.5em;\n  transform: rotate(180deg);\n  line-height: 1;\n  text-align: center;\n  color: #ffff00; }\n\n.cube > div:first-child {\n  transform: rotateX(90deg) translateZ(5em); }\n\n.cube > div:nth-child(2) {\n  transform: translateZ(5em); }\n\n.cube > div:nth-child(3) {\n  transform: rotateY(90deg) translateZ(5em); }\n\n.cube > div:nth-child(4) {\n  transform: rotateY(180deg) translateZ(5em); }\n\n.cube > div:nth-child(5) {\n  transform: rotateY(-90deg) translateZ(5em); }\n\n.cube > div:nth-child(6) {\n  transform: rotateX(-90deg) rotate(180deg) translateZ(5em); }\n\n.cube.index-1 {\n  transform: rotateX(280deg) rotateY(185deg) rotateZ(0deg); }\n\n.cube.index-2 {\n  transform: rotateX(10deg) rotateY(5deg) rotateZ(180deg); }\n\n.cube.index-3 {\n  transform: rotateX(100deg) rotateY(95deg) rotateZ(90deg); }\n\n.cube.index-4 {\n  transform: rotateX(190deg) rotateY(5deg) rotateZ(0deg); }\n\n.cube.index-5 {\n  transform: rotateX(10deg) rotateY(275deg) rotateZ(180deg); }\n\n.cube.index-6 {\n  transform: rotateX(280deg) rotateY(185deg) rotateZ(180deg); }\n\n.cube.bis.index-1 {\n  transform: rotateX(610deg) rotateY(545deg) rotateZ(365deg); }\n\n.cube.bis.index-2 {\n  transform: rotateX(340deg) rotateY(365deg) rotateZ(545deg); }\n\n.cube.bis.index-3 {\n  transform: rotateX(430deg) rotateY(455deg) rotateZ(455deg); }\n\n.cube.bis.index-4 {\n  transform: rotateX(520deg) rotateY(365deg) rotateZ(365deg); }\n\n.cube.bis.index-5 {\n  transform: rotateX(340deg) rotateY(635deg) rotateZ(545deg); }\n\n.cube.bis.index-6 {\n  transform: rotateX(610deg) rotateY(545deg) rotateZ(545deg); }\n\n.buttons {\n  display: block;\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0; }\n\n.buttons button {\n  display: inline-block;\n  border: none;\n  border-radius: 0.2em;\n  font-size: 1.5em;\n  outline: none;\n  background: #fa2d7d;\n  color: #FFF;\n  padding: 0.5em 1em;\n  cursor: pointer; }\n\n.buttons button:active {\n  transform: scale(0.9); }\n\n.buttons button + button {\n  display: none; }\n\nh3 {\n  font-family: 'Quicksand', sans-serif;\n  color: #383838;\n  text-align: center;\n  font-size: 13px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYXJ0aW5hL0Rlc2t0b3AvcGljbmljL3NyYy9hcHAvZGFkaS9kYWRpLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9kYWRpL2RhZGkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBb0M7RUFDcEMsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQix5QkFBaUI7S0FBakIsc0JBQWlCO01BQWpCLHFCQUFpQjtVQUFqQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxjQUFjLEVBQUE7O0FDRWxCO0VER0ksYUFBYTtFQUNiLDZDQUE2QyxFQUFBOztBQVcvQztFQUNFLGtCQUFrQjtFQUdsQixxQkFBcUI7RUFHckIsdUNBQXVDO0VBQ3ZDLGtCQUFrQixFQUFBOztBQUlwQjtFQUNFLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLFlBeEJTO0VBeUJULFdBekJTO0VBMEJULDRCQUE0QjtFQUU1QixvREFBb0Q7RUFDcEQsaUNBdkJvQjtFQXlCcEIscUJBQXFCO0VBQ3JCLFdBQVcsRUFBQTs7QUFJYjtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0NBQWtDO0VBQ2xDLDRCQUE0QjtFQUM1Qix3QkFBd0I7RUFDeEIsWUExQ1M7RUEyQ1QsV0EzQ1M7RUE0Q1QsbUJBQW1CO0VBQ25CLHlCQUFpQjtLQUFqQixzQkFBaUI7TUFBakIscUJBQWlCO1VBQWpCLGlCQUFpQjtFQUNqQixzQ0FBbUM7RUFDbkMsb0JBQW9CO0VBRXBCLDhCQUE4QjtFQUU5QixlQUFlLEVBQUE7O0FBZmpCO0lBa0JPLDZDQUF5RCxFQUFBOztBQWxCaEU7SUFxQlEsNkNBQXlELEVBQUE7O0FBckJqRTtJQXdCUSw2Q0FBeUQsRUFBQTs7QUF4QmpFO0lBMkJRLDZDQUF5RCxFQUFBOztBQTNCakU7SUE4QlEsNkNBQXlELEVBQUE7O0FBOUJqRTtJQWlDUSw2Q0FBeUQsRUFBQTs7QUFNakU7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFBRSxXQUFXO0VBQ3ZCLGtCQUFrQjtFQUNsQixRQUFPO0VBQUUsU0FBUztFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGNBQWMsRUFBQTs7QUFHaEI7RUFHSSx5Q0FBMkMsRUFBQTs7QUFIL0M7RUFNSSwwQkFBNEIsRUFBQTs7QUFOaEM7RUFTSSx5Q0FBMkMsRUFBQTs7QUFUL0M7RUFZSSwwQ0FBNEMsRUFBQTs7QUFaaEQ7RUFlSSwwQ0FBNEMsRUFBQTs7QUFmaEQ7RUFrQkkseURBQTJELEVBQUE7O0FBSy9EO0VBS2Msd0RBQStFLEVBQUE7O0FBTDdGO0VBTWMsdURBQWlGLEVBQUE7O0FBTi9GO0VBT2Msd0RBQWdGLEVBQUE7O0FBUDlGO0VBUWMsc0RBQStFLEVBQUE7O0FBUjdGO0VBU2MseURBQWlGLEVBQUE7O0FBVC9GO0VBVWMsMERBQWlGLEVBQUE7O0FBVi9GO0VBaUJnQiwwREFBK0UsRUFBQTs7QUFqQi9GO0VBa0JnQiwwREFBaUYsRUFBQTs7QUFsQmpHO0VBbUJnQiwwREFBZ0YsRUFBQTs7QUFuQmhHO0VBb0JnQiwwREFBK0UsRUFBQTs7QUFwQi9GO0VBcUJnQiwwREFBaUYsRUFBQTs7QUFyQmpHO0VBc0JnQiwwREFBaUYsRUFBQTs7QUFRakc7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUVsQixPQUFPO0VBQUUsUUFBTyxFQUFBOztBQUlsQjtFQUNFLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUdqQjtFQUNFLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLG9DQUFvQztFQUNwQyxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2RhZGkvZGFkaS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHl7XG4gICAgZm9udC1mYW1pbHk6ICdXb3JrIFNhbnMnLCBzYW5zLXNlcmlmO1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAgIGNvbG9yOiAjZmEyZDdkO1xuICAgIGNvbG9yOiAjNTc1MzQ0O1xuICB9XG5cbiAgXG4gIFtvbmNsaWNrXXtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwKTsgXG4gIH1cbiAgXG4gICRzaXplOiAxMGVtO1xuICAkaGFsZjogJHNpemUqLjU7XG4gIFxuICBcbiAgJHNwaW4tZHVyYXRpb246IDFzO1xuICAkc3Bpbi1lYXNpbmc6IGN1YmljLWJlemllcigwLDAuNCwwLjQsMS4wMjUpO1xuICAkc3Bpbi1lYXNpbmc6IGVhc2Utb3V0O1xuICBcbiAgLnZpZXdwb3J0IHtcbiAgICBwZXJzcGVjdGl2ZTogMzUwcHg7XG4gICAgLy9wZXJzcGVjdGl2ZS1vcmlnaW46IDUwJSA1MCU7XG4gICAgLy90cmFuc2Zvcm06IHNjYWxlKDAuOCwgMC44KTtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XG4gICAgXG4gICAgXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpLCBzY2FsZSgwLjcpO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBcbiAgfVxuICBcbiAgLmN1YmUge1xuICAgIGZvbnQtc2l6ZTogLjVlbTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgaGVpZ2h0OiAkc2l6ZTtcbiAgICB3aWR0aDogJHNpemU7XG4gICAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcbiAgICAvL3RyYW5zZm9ybTogcm90YXRlWCgxMzZkZWcpIHJvdGF0ZVkoMTEyMmRlZykgcm90YXRlWigwZGVnKTtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoMGRlZykgcm90YXRlWSgwZGVnKSByb3RhdGVaKDBkZWcpO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAkc3Bpbi1kdXJhdGlvbiAkc3Bpbi1lYXNpbmc7XG4gICAgXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogNGVtO1xuICBcbiAgfVxuICBcbiAgLmN1YmUgPiBkaXYge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgaGVpZ2h0OiAkc2l6ZTtcbiAgICB3aWR0aDogJHNpemU7XG4gICAgYm9yZGVyOiAwLjNlbSBzb2xpZDtcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICBib3gtc2hhZG93OiAwIDAgMWVtIHJnYmEoMCwwLDAsMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjVlbTtcbiAgICBcbiAgICBvdXRsaW5lOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgXG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICYuc2lkZXtcbiAgICAgICAgJi0xe1xuICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTp1cmwoJy4uLy4uL2Fzc2V0cy9mYWNjaWFfZGl0YV8wMS1kLnBuZycpOyAgIFxuICAgICAgICB9XG4gICAgICAgICYtMntcbiAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOnVybCgnLi4vLi4vYXNzZXRzL2ZhY2NpYV9kaXRhXzAyLWQucG5nJyk7ICAgXG4gICAgICAgICB9XG4gICAgICAgICAmLTN7XG4gICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTp1cmwoJy4uLy4uL2Fzc2V0cy9mYWNjaWFfZGl0YV8wMy1kLnBuZycpOyAgIFxuICAgICAgICAgfVxuICAgICAgICAgJi00e1xuICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6dXJsKCcuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDQtZC5wbmcnKTsgICBcbiAgICAgICAgIH1cbiAgICAgICAgICYtNXtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOnVybCgnLi4vLi4vYXNzZXRzL2ZhY2NpYV9kaXRhXzA1LWQucG5nJyk7ICAgXG4gICAgICAgICB9XG4gICAgICAgICAmLTZ7XG4gICAgICAgICAgYmFja2dyb3VuZC1pbWFnZTp1cmwoJy4uLy4uL2Fzc2V0cy9mYWNjaWFfZGl0YV8wNi1kLnBuZycpOyAgIFxuICAgICAgICAgfVxuICAgICAgICB9XG4gIH1cbiAgXG4gIFxuICAuY3ViZSA+IGRpdiA+IGRpdntcbiAgICBmb250LXNpemU6IDRlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHdpZHRoOiAxZW07IGhlaWdodDogMWVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6NTAlOyBsZWZ0OiA1MCU7XG4gICAgbWFyZ2luLXRvcDogLS41ZW07XG4gICAgbWFyZ2luLWxlZnQ6IC0uNWVtO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmZjAwO1xuICB9XG4gIFxuICAuY3ViZSA+IGRpdntcbiAgICBcbiAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWCg5MGRlZykgdHJhbnNsYXRlWigkaGFsZik7XG4gICAgfVxuICAgICY6bnRoLWNoaWxkKDIpIHtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWigkaGFsZik7XG4gICAgfVxuICAgICY6bnRoLWNoaWxkKDMpIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWSg5MGRlZykgdHJhbnNsYXRlWigkaGFsZik7XG4gICAgfVxuICAgICY6bnRoLWNoaWxkKDQpIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWSgxODBkZWcpIHRyYW5zbGF0ZVooJGhhbGYpO1xuICAgIH1cbiAgICAmOm50aC1jaGlsZCg1KSB7XG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZVkoLTkwZGVnKSB0cmFuc2xhdGVaKCRoYWxmKTtcbiAgICB9XG4gICAgJjpudGgtY2hpbGQoNikge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGVYKC05MGRlZykgcm90YXRlKDE4MGRlZykgdHJhbnNsYXRlWigkaGFsZik7XG4gICAgfVxuICAgIFxuICB9XG4gIFxuICAuY3ViZXtcbiAgXG4gICAgJEY6IDA7IFxuICAgICRPWDogMTA7ICRPWTogNTsgJE9aOiAwO1xuICAgICAgXG4gICAgJi5pbmRleC0xeyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCsyNzBkZWcpICByb3RhdGVZKCRGKyRPWSsxODBkZWcpICByb3RhdGVaKCRGKyRPWiswZGVnKSAgICB9XG4gICAgJi5pbmRleC0yeyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCswZGVnKSAgICByb3RhdGVZKCRGKyRPWSswZGVnKSAgICByb3RhdGVaKCRGKyRPWisxODBkZWcpICB9XG4gICAgJi5pbmRleC0zeyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCs5MGRlZykgICByb3RhdGVZKCRGKyRPWSs5MGRlZykgICByb3RhdGVaKCRGKyRPWis5MGRlZykgICB9XG4gICAgJi5pbmRleC00eyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCsxODBkZWcpICByb3RhdGVZKCRGKyRPWSswZGVnKSAgICByb3RhdGVaKCRGKyRPWiswZGVnKSAgICB9XG4gICAgJi5pbmRleC01eyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCswZGVnKSAgICByb3RhdGVZKCRGKyRPWSsyNzBkZWcpICByb3RhdGVaKCRGKyRPWisxODBkZWcpICB9XG4gICAgJi5pbmRleC02eyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCsyNzBkZWcgKSByb3RhdGVZKCRGKyRPWSsxODBkZWcpICByb3RhdGVaKCRGKyRPWisxODBkZWcpICB9XG4gICAgXG4gICAgJi5iaXN7XG4gIFxuICAgICAgJEY6IDM2MDsgXG4gICAgICAkT1g6IC0yMDsgJE9ZOiA1OyAkT1o6IDU7XG4gIFxuICAgICAgJi5pbmRleC0xeyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCsyNzBkZWcpICByb3RhdGVZKCRGKyRPWSsxODBkZWcpICByb3RhdGVaKCRGKyRPWiswZGVnKSAgICB9XG4gICAgICAmLmluZGV4LTJ7ICB0cmFuc2Zvcm06IHJvdGF0ZVgoJEYrJE9YKzBkZWcpICAgIHJvdGF0ZVkoJEYrJE9ZKzBkZWcpICAgIHJvdGF0ZVooJEYrJE9aKzE4MGRlZykgIH1cbiAgICAgICYuaW5kZXgtM3sgIHRyYW5zZm9ybTogcm90YXRlWCgkRiskT1grOTBkZWcpICAgcm90YXRlWSgkRiskT1krOTBkZWcpICAgcm90YXRlWigkRiskT1orOTBkZWcpICAgfVxuICAgICAgJi5pbmRleC00eyAgdHJhbnNmb3JtOiByb3RhdGVYKCRGKyRPWCsxODBkZWcpICByb3RhdGVZKCRGKyRPWSswZGVnKSAgICByb3RhdGVaKCRGKyRPWiswZGVnKSAgICB9XG4gICAgICAmLmluZGV4LTV7ICB0cmFuc2Zvcm06IHJvdGF0ZVgoJEYrJE9YKzBkZWcpICAgIHJvdGF0ZVkoJEYrJE9ZKzI3MGRlZykgIHJvdGF0ZVooJEYrJE9aKzE4MGRlZykgIH1cbiAgICAgICYuaW5kZXgtNnsgIHRyYW5zZm9ybTogcm90YXRlWCgkRiskT1grMjcwZGVnICkgcm90YXRlWSgkRiskT1krMTgwZGVnKSAgcm90YXRlWigkRiskT1orMTgwZGVnKSAgfVxuICBcbiAgICB9XG4gICAgXG4gIH1cblxuICBcbiAgXG4gIC5idXR0b25ze1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgXG4gICAgbGVmdDogMDsgcmlnaHQ6MDtcbiAgfVxuICBcbiAgXG4gIC5idXR0b25zIGJ1dHRvbntcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMmVtO1xuICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiAjZmEyZDdkO1xuICAgIGNvbG9yOiAjRkZGO1xuICAgIHBhZGRpbmc6IDAuNWVtIDFlbTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gIH1cbiAgXG4gIC5idXR0b25zIGJ1dHRvbjphY3RpdmV7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjkpO1xuICB9XG4gIFxuICAuYnV0dG9ucyBidXR0b24gKyBidXR0b257XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGgze1xuICAgIGZvbnQtZmFtaWx5OiAnUXVpY2tzYW5kJywgc2Fucy1zZXJpZjtcbiAgICBjb2xvcjogIzM4MzgzODtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9IiwiYm9keSB7XG4gIGZvbnQtZmFtaWx5OiAnV29yayBTYW5zJywgc2Fucy1zZXJpZjtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGNvbG9yOiAjZmEyZDdkO1xuICBjb2xvcjogIzU3NTM0NDsgfVxuXG5bb25jbGlja10ge1xuICBvdXRsaW5lOiBub25lO1xuICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMCk7IH1cblxuLnZpZXdwb3J0IHtcbiAgcGVyc3BlY3RpdmU6IDM1MHB4O1xuICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKSwgc2NhbGUoMC43KTtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5jdWJlIHtcbiAgZm9udC1zaXplOiAuNWVtO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBoZWlnaHQ6IDEwZW07XG4gIHdpZHRoOiAxMGVtO1xuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xuICB0cmFuc2Zvcm06IHJvdGF0ZVgoMGRlZykgcm90YXRlWSgwZGVnKSByb3RhdGVaKDBkZWcpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMXMgZWFzZS1vdXQ7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiA0ZW07IH1cblxuLmN1YmUgPiBkaXYge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgaGVpZ2h0OiAxMGVtO1xuICB3aWR0aDogMTBlbTtcbiAgYm9yZGVyOiAwLjNlbSBzb2xpZDtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAxZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBib3JkZXItcmFkaXVzOiAwLjVlbTtcbiAgb3V0bGluZTogMXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBjdXJzb3I6IHBvaW50ZXI7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS0xIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDEtZC5wbmdcIik7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS0yIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDItZC5wbmdcIik7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS0zIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDMtZC5wbmdcIik7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS00IHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDQtZC5wbmdcIik7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS01IHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDUtZC5wbmdcIik7IH1cbiAgLmN1YmUgPiBkaXYuc2lkZS02IHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvZmFjY2lhX2RpdGFfMDYtZC5wbmdcIik7IH1cblxuLmN1YmUgPiBkaXYgPiBkaXYge1xuICBmb250LXNpemU6IDRlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgbWFyZ2luLXRvcDogLS41ZW07XG4gIG1hcmdpbi1sZWZ0OiAtLjVlbTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmMDA7IH1cblxuLmN1YmUgPiBkaXY6Zmlyc3QtY2hpbGQge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVgoOTBkZWcpIHRyYW5zbGF0ZVooNWVtKTsgfVxuXG4uY3ViZSA+IGRpdjpudGgtY2hpbGQoMikge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVooNWVtKTsgfVxuXG4uY3ViZSA+IGRpdjpudGgtY2hpbGQoMykge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVkoOTBkZWcpIHRyYW5zbGF0ZVooNWVtKTsgfVxuXG4uY3ViZSA+IGRpdjpudGgtY2hpbGQoNCkge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVkoMTgwZGVnKSB0cmFuc2xhdGVaKDVlbSk7IH1cblxuLmN1YmUgPiBkaXY6bnRoLWNoaWxkKDUpIHtcbiAgdHJhbnNmb3JtOiByb3RhdGVZKC05MGRlZykgdHJhbnNsYXRlWig1ZW0pOyB9XG5cbi5jdWJlID4gZGl2Om50aC1jaGlsZCg2KSB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCgtOTBkZWcpIHJvdGF0ZSgxODBkZWcpIHRyYW5zbGF0ZVooNWVtKTsgfVxuXG4uY3ViZS5pbmRleC0xIHtcbiAgdHJhbnNmb3JtOiByb3RhdGVYKDI4MGRlZykgcm90YXRlWSgxODVkZWcpIHJvdGF0ZVooMGRlZyk7IH1cblxuLmN1YmUuaW5kZXgtMiB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCgxMGRlZykgcm90YXRlWSg1ZGVnKSByb3RhdGVaKDE4MGRlZyk7IH1cblxuLmN1YmUuaW5kZXgtMyB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCgxMDBkZWcpIHJvdGF0ZVkoOTVkZWcpIHJvdGF0ZVooOTBkZWcpOyB9XG5cbi5jdWJlLmluZGV4LTQge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVgoMTkwZGVnKSByb3RhdGVZKDVkZWcpIHJvdGF0ZVooMGRlZyk7IH1cblxuLmN1YmUuaW5kZXgtNSB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCgxMGRlZykgcm90YXRlWSgyNzVkZWcpIHJvdGF0ZVooMTgwZGVnKTsgfVxuXG4uY3ViZS5pbmRleC02IHtcbiAgdHJhbnNmb3JtOiByb3RhdGVYKDI4MGRlZykgcm90YXRlWSgxODVkZWcpIHJvdGF0ZVooMTgwZGVnKTsgfVxuXG4uY3ViZS5iaXMuaW5kZXgtMSB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCg2MTBkZWcpIHJvdGF0ZVkoNTQ1ZGVnKSByb3RhdGVaKDM2NWRlZyk7IH1cblxuLmN1YmUuYmlzLmluZGV4LTIge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVgoMzQwZGVnKSByb3RhdGVZKDM2NWRlZykgcm90YXRlWig1NDVkZWcpOyB9XG5cbi5jdWJlLmJpcy5pbmRleC0zIHtcbiAgdHJhbnNmb3JtOiByb3RhdGVYKDQzMGRlZykgcm90YXRlWSg0NTVkZWcpIHJvdGF0ZVooNDU1ZGVnKTsgfVxuXG4uY3ViZS5iaXMuaW5kZXgtNCB7XG4gIHRyYW5zZm9ybTogcm90YXRlWCg1MjBkZWcpIHJvdGF0ZVkoMzY1ZGVnKSByb3RhdGVaKDM2NWRlZyk7IH1cblxuLmN1YmUuYmlzLmluZGV4LTUge1xuICB0cmFuc2Zvcm06IHJvdGF0ZVgoMzQwZGVnKSByb3RhdGVZKDYzNWRlZykgcm90YXRlWig1NDVkZWcpOyB9XG5cbi5jdWJlLmJpcy5pbmRleC02IHtcbiAgdHJhbnNmb3JtOiByb3RhdGVYKDYxMGRlZykgcm90YXRlWSg1NDVkZWcpIHJvdGF0ZVooNTQ1ZGVnKTsgfVxuXG4uYnV0dG9ucyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7IH1cblxuLmJ1dHRvbnMgYnV0dG9uIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDAuMmVtO1xuICBmb250LXNpemU6IDEuNWVtO1xuICBvdXRsaW5lOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjZmEyZDdkO1xuICBjb2xvcjogI0ZGRjtcbiAgcGFkZGluZzogMC41ZW0gMWVtO1xuICBjdXJzb3I6IHBvaW50ZXI7IH1cblxuLmJ1dHRvbnMgYnV0dG9uOmFjdGl2ZSB7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC45KTsgfVxuXG4uYnV0dG9ucyBidXR0b24gKyBidXR0b24ge1xuICBkaXNwbGF5OiBub25lOyB9XG5cbmgzIHtcbiAgZm9udC1mYW1pbHk6ICdRdWlja3NhbmQnLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzM4MzgzODtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEzcHg7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/dadi/dadi.component.ts":
/*!****************************************!*\
  !*** ./src/app/dadi/dadi.component.ts ***!
  \****************************************/
/*! exports provided: DadiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DadiComponent", function() { return DadiComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DadiComponent = /** @class */ (function () {
    function DadiComponent() {
        this.dadi = new Array('uno');
        this.animation = new Array(true, false);
        this.status = new Array('index-1', 'index-1');
    }
    DadiComponent.prototype.ngOnInit = function () {
    };
    DadiComponent.prototype.rollToIndex = function () {
        var _this = this;
        this.dadi.forEach(function (dado, index) {
            var r_number = Math.floor(Math.random() * 6) + 1;
            _this.status[index] = 'index-' + r_number;
            _this.animation[index] = !_this.animation[index];
        });
    };
    DadiComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dadi',
            template: __webpack_require__(/*! ./dadi.component.html */ "./src/app/dadi/dadi.component.html"),
            styles: [__webpack_require__(/*! ./dadi.component.scss */ "./src/app/dadi/dadi.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DadiComponent);
    return DadiComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/martina/Desktop/picnic/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map